package com.sda.gym.dao;

import com.sda.gym.model.Subscription;
import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class SubscriptionDao {
    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public void updateSubscription (Subscription subscription){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(subscription);
        tx.commit();
        session.close();
    }

    public void deleteSubscription(Subscription subscription) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(subscription);
        tx.commit();
        session.close();
    }

    public void createSubscription(Subscription subscription) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(subscription);
        tx.commit();
        session.close();
    }
}
