package com.sda.gym.dao;

import com.sda.gym.model.ExercisePlan;
import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ExercisePlanDao {
    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public void updateExercisePlan(ExercisePlan exercisePlan) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(exercisePlan);
        tx.commit();
        session.close();
    }

    public void deleteExercisePlan(ExercisePlan exercisePlan) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(exercisePlan);
        tx.commit();
        session.close();
    }

    public void createExercisePlan(ExercisePlan exercisePlan) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(exercisePlan);
        tx.commit();
        session.close();
    }

    public int getIdByName(String name) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("select id from ExercisePlan where name=: name");
        query.setParameter("name", name);
        return (int) query.uniqueResult();
    }

    public ExercisePlan getExercisePlanById(long id){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        ExercisePlan exercisePlan = session.find(ExercisePlan.class,id);
        tx.commit();
        session.close();
        return exercisePlan;
    }
}
