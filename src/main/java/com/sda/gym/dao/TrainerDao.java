package com.sda.gym.dao;

import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class TrainerDao {

    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Trainer> getAllTrainers() {
        //TODO: use Session and Transaction
        // Query the DB using SQL, HQL or Criteria

        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query<Trainer> query = session.createQuery("from Trainer");
        List<Trainer> trainers = query.list();
        tx.commit();
        session.close();
        return trainers;
    }

    public List<Trainer> getTrainerByName(String name) {
        Session session = sessionFactory.openSession();
        Transaction tx1 = session.beginTransaction();
        Query<Trainer> query = session.createQuery("from Trainer where name=: name");
        query.setParameter("name", name);
        List<Trainer> trainer = query.list();
        tx1.commit();
        session.close();
        return trainer;
    }

    public void updateTrainer(Trainer trainer) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(trainer);
        tx.commit();
        session.close();
    }

    public void updateNameById(long id, String name) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Trainer trainer = (Trainer) session.get(Trainer.class, id);
        trainer.setName(name);
        session.update(trainer);
        tx.commit();
        session.close();
    }

    public Trainer getTrainerById(long id) {
        Session session = sessionFactory.openSession();
        Transaction tx1 = session.beginTransaction();
        Trainer trainer = (Trainer) session.find(Trainer.class, id);
        tx1.commit();
        session.close();
        return trainer;
    }

    public void deleteTrainer(Trainer trainer) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(trainer);
        tx.commit();
        session.close();
    }

    public void deleteTrainer(long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("delete from Trainer where id=: id");
        query.setParameter("id", id);
        query.executeUpdate();
        tx.commit();
        session.close();
    }

    public void createTrainer(Trainer trainer) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(trainer);
        tx.commit();
        session.close();
    }
}