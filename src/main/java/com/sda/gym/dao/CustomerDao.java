package com.sda.gym.dao;

import com.sda.gym.model.Customer;
import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;


public class CustomerDao {
    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public List<Customer> getAllCustomers() {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query<Customer> query = session.createQuery("from Customer");
        List<Customer> customers = query.list();
        tx.commit();
        session.close();
        return customers;
    }

    public void deleteAllCustomers(){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.createNativeQuery("delete from Customer").executeUpdate();
        tx.commit();
        session.close();
    }

    public Customer getCustomerById(long id) {
        Session session = sessionFactory.openSession();
        Transaction tx1 = session.beginTransaction();
        Customer customer = (Customer) session.find(Customer.class, id);
        tx1.commit();
        session.close();
        return customer;
    }

    public List<Customer> getCustomerByName(String name) {
        Session session = sessionFactory.openSession();
        Transaction tx1 = session.beginTransaction();
        Query<Customer> query = session.createQuery("from Customer where name=: name");
        query.setParameter("name", name);
        List<Customer> customer = query.list();
        tx1.commit();
        session.close();
        return customer;
    }

    public void deleteCustomer(long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("delete from Customer where id=: id");
        query.setParameter("id", id);
        tx.commit();
        session.close();
    }

    public void createCustomer(Customer customer) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(customer);
        tx.commit();
        session.close();
    }

    public void updateCustomer(Customer customer) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(customer);
        tx.commit();
        session.close();
    }

    public void updateNameById(long id, String name) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Customer customer = (Customer) session.get(Customer.class, id);
        customer.setName(name);
        session.update(customer);
        tx.commit();
        session.close();
    }

    public void deleteCustomer(Customer customer) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(customer);
        tx.commit();
        session.close();
    }
}
