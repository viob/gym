package com.sda.gym.dao;

import com.sda.gym.model.Equipment;
import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class EquipmentDao {
    private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public void updateEquipment(Equipment equipment) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(equipment);
        tx.commit();
        session.close();
    }

    public void deleteEquipment(Equipment equipment) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.delete(equipment);
        tx.commit();
        session.close();
    }

    public Equipment createEquipment(Equipment equipment) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(equipment);
        tx.commit();
        session.close();
        return equipment;
    }

    public Equipment getEquipmentById (long id){
        Session session =sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        Equipment equipment = (Equipment) session.find(Equipment.class, id);
        tx.commit();
        session.close();
        return equipment;
    }
}
