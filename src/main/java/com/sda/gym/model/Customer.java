package com.sda.gym.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "customer")

public class Customer {

    public  Customer(){}

    public Customer(String adress, String name, String phone, int age){
        this.adress = adress;
        this.name = name;
        this.phone = phone;
        this.age = age;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "customer")
    private Set<Subscription> subscriptions;

    @Column(name = "age")
    private int age;

    @Column(name = "nume")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "adress")
    private String adress;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Customer: " +
                "id= " + id +
                ", name= " + name + '\'' +
                ", phone= " + phone +
                ", adress= " + adress+
                ", age= " + age;
    }
}
