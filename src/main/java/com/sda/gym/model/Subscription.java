package com.sda.gym.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "subscription")

public class Subscription {

    public Subscription(Trainer trainer, String dateTime, Customer customer, ExercisePlan exercisePlan){
        this.dateTime = dateTime;
        this.trainer = trainer;
        this.customer = customer;
        this.exercisePlan = exercisePlan;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "date_time")
    private String dateTime;

    @ManyToOne
    @JoinColumn(name = "trainer_id", nullable = false)
    private Trainer trainer;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="exercise_plan_id", nullable=false)
    private ExercisePlan exercisePlan;



    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    @Override
    public String toString() {
        return "Subscription: " +
                "id= " + id +
                ", dateTime= " + dateTime + '\'' +
                ", trainer= " + trainer;
    }
}
