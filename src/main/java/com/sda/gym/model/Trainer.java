package com.sda.gym.model;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "trainer")

public class Trainer {

    public Trainer() {}
    public Trainer(String name){
        this.name = name;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "trainer")
    private Set<Subscription> subscriptions;

    @Column(name = "nume")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Trainer: " +
                "id= " + id +
                ", name= " + name;
    }

    // Trainer properties / instance variables
    // toString, hashCode, equals ?
}
