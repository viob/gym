package com.sda.gym.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "equipment")

public class Equipment {

    public Equipment(){}

    public Equipment(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "equipments")
    private Set<ExercisePlan> exercisePlans = new HashSet<>();

    @Column(name = "nume")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExercisePlans(Set<ExercisePlan> exercisePlans) {
        this.exercisePlans = exercisePlans;
    }

    public Set<ExercisePlan> getExercisePlans() {
        return exercisePlans;
    }

    @Override
    public String toString() {
        return "Equipment: " +
                "id= " + id +
                ", name= " + name;
    }
}
