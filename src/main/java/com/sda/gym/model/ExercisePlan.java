package com.sda.gym.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "exercise_plan")

public class ExercisePlan {

    public ExercisePlan(){}

    public ExercisePlan(int time) {
        this.time = time;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "exercisePlan")
    private Set<Subscription> subscriptions;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "equipment_exercise_plan",
            joinColumns = @JoinColumn(name = "equipment_id"),
            inverseJoinColumns = @JoinColumn(name = "exercise_plan_id")
    )

    private Set<Equipment> equipments = new HashSet<>();

    @Column(name = "time")
    private int time;

    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> equipments) {
        this.equipments = equipments;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ExercisePlan: " +
                "id= " + id +
                ", time= " + time;
    }
}
