package com.sda.gym.service;

import com.sda.gym.dao.SubscriptionDao;
import com.sda.gym.model.Subscription;
import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class SubscriptionService {
    private SubscriptionDao subscriptionDao = new SubscriptionDao();

    public void updateSubscription(Subscription subscription) {
        subscriptionDao.updateSubscription(subscription);
    }

    public void deleteSubscription(Subscription subscription) {
        subscriptionDao.deleteSubscription(subscription);
    }

    public void createSubscription(Subscription subscription) {
        subscriptionDao.createSubscription(subscription);
    }
}
