package com.sda.gym.service;

import com.sda.gym.dao.EquipmentDao;
import com.sda.gym.model.Equipment;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class EquipmentService {

    private EquipmentDao equipmentDao = new EquipmentDao();

    public void updateEquipment(Equipment equipment) {
        equipmentDao.updateEquipment(equipment);
    }

    public void deleteEquipment(Equipment equipment) {
        equipmentDao.deleteEquipment(equipment);
    }

    public Equipment createEquipment(Equipment equipment) {
        return equipmentDao.createEquipment(equipment);
    }
    public Equipment getEquipmentById(long id){
        return equipmentDao.getEquipmentById(id);
    }
}
