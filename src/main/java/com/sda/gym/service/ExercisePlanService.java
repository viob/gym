package com.sda.gym.service;

import com.sda.gym.dao.ExercisePlanDao;
import com.sda.gym.model.ExercisePlan;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ExercisePlanService {
    private ExercisePlanDao exercisePlanDao = new ExercisePlanDao();

    public void updateExercisePlan(ExercisePlan exercisePlan) {
        exercisePlanDao.updateExercisePlan(exercisePlan);
    }

    public void deleteExercisePlan(ExercisePlan exercisePlan) {
        exercisePlanDao.deleteExercisePlan(exercisePlan);
    }

    public void createExercisePlan(ExercisePlan exercisePlan) {
        exercisePlanDao.createExercisePlan(exercisePlan);
    }
    public void getIdByName(String name){
        exercisePlanDao.getIdByName(name);
    }
    public ExercisePlan getExercisePlanById(long id){
        return exercisePlanDao.getExercisePlanById(id);
    }
}