package com.sda.gym.service;

import com.sda.gym.dao.TrainerDao;
import com.sda.gym.model.Trainer;

import java.util.List;

public class TrainerService {

    private TrainerDao trainerDao = new TrainerDao();

// CRUD Operations + business logic

    public List<Trainer> getAllTrainers() {
        return trainerDao.getAllTrainers();
    }

    public void createTrainer(Trainer trainer) {
        trainerDao.createTrainer(trainer);
    }

    public void deleteTrainer(Trainer trainer) {
        trainerDao.deleteTrainer(trainer);
    }

    public void deleteTrainer(long id) {
        trainerDao.deleteTrainer(id);
    }

    public Trainer getTrainerById(long id) {
        return trainerDao.getTrainerById(id);

    }

    public List<Trainer> getTrainerByName(String name) {
        return trainerDao.getTrainerByName(name);
    }

    public void updateNameById(long id, String name) {
        trainerDao.updateNameById(id, name);
    }

    public void updateTrainer(Trainer trainer) {
         trainerDao.updateTrainer(trainer);
    }
}
