package com.sda.gym.service;

import com.sda.gym.dao.CustomerDao;
import com.sda.gym.dao.TrainerDao;
import com.sda.gym.model.Customer;
import com.sda.gym.model.Trainer;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CustomerService {

    private CustomerDao customerDao = new CustomerDao();

    public List<Customer> getAllCustomers() {
        return customerDao.getAllCustomers();
    }

    public List<Customer> getCustomerByName(String name) {
        return customerDao.getCustomerByName(name);
    }

    public void deleteCustomer(long id) {
        customerDao.deleteCustomer(id);
    }

    public void createCustomer(Customer customer) {
        customerDao.createCustomer(customer);
    }

    public void updateCustomer(Customer customer) {
        customerDao.updateCustomer(customer);
    }

    public void updateNameById(long id, String name) {
        customerDao.updateNameById(id, name);
    }

    public Customer getCustomerById(long id) {
        return customerDao.getCustomerById(id);
    }

    public void deleteCustomer(Customer customer) {
        customerDao.deleteCustomer(customer);
    }

    public Customer getOldestCustomer() {
        List<Customer> getAllCustomers = customerDao.getAllCustomers();
        Customer oldestCustomer = getAllCustomers.get(0);
        for (Customer customer : getAllCustomers) {
            if (oldestCustomer.getAge() < customer.getAge()) {
                oldestCustomer = customer;
            }
        }
        return oldestCustomer;
    }
    public void deleteAllCustomers(){
        customerDao.deleteAllCustomers();
    }
}
